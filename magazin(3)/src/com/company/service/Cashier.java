package com.company.service;

import com.company.departments.BaseDepartment;

public class Cashier extends BaseEmployee{
    private boolean free;

    public Cashier(String name,boolean free,BaseDepartment department){
        super(name, free, department);
    }

    public void getMoney(){}

    public void giveBonusCard(){}

    public boolean isFree(){
        return free;
    }
}
