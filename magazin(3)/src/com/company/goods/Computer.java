package com.company.goods;

import com.company.interfaces.IDepartment;

public class Computer extends ElectronicDevice {
    private String ram;

    public Computer(IDepartment department, String name, boolean hasGuarantee, String price, String company, String ram) {
        super(department, name, hasGuarantee, price, company);
        this.ram = ram;
    }

    public Computer(String name, String ram) {
        super(name);
        this.ram = ram;
    }

    public Computer(String name){
        super(name);
    }

    public void on() {}

    public void off() {}

    public void loadOS() {}
}


