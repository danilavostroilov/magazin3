package com.company.clients;

import com.company.interfaces.IGood;
import com.company.interfaces.IVisitor;

public abstract class BaseVisitor implements IVisitor {
    private String name;

    public BaseVisitor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void buy(IGood good){
        System.out.println(good.getName());
    }

    public void returnGoods(){}

}

