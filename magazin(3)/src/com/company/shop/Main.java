package com.company.start;

import com.company.clients.Visitor;
import com.company.goods.Computer;
import com.company.goods.Televisor;

public class Main {

    public static void main(String[] args) {

        Computer computer =new Computer("Компьютер");
        Televisor televisor=new Televisor("Телевизор");
        Visitor visitor=new Visitor("Клиент");

        visitor.buy(computer);

    }
}

