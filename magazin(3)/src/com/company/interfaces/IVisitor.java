package com.company.interfaces;

public interface IVisitor {
    void buy(IGood good);
    void returnGoods();
    String getName();
}


