package com.company.interfaces;

public interface IGood {
    IDepartment getDepartment();
    String getCompany();
    String getName();
    String getPrice();
    boolean isHasGuarantee();
}


